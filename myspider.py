import scrapy

class MySpider(scrapy.Spider):
    name = 'myspider'
    start_urls = [
        'http://www.imdb.com/chart/top'
    ]

    def parse(self, response):

        for link in response.xpath('//td/a/@href').extract():
            yield {"Link": link}
